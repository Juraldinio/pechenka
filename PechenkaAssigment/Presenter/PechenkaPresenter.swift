//
//  PechenkaPresenter.swift
//  PechenkaAssigment
//
//  Created by Juraldinio on 01/04/2019.
//  Copyright © 2019 Juraldinio. All rights reserved.
//

import Foundation

final class PechenkaPresenter {
    
    private let interactor: PechenkaInteractorInput
    weak var viewInput: PechenkaViewControllerInput?
    
    init(with interactor: PechenkaInteractorInput) {
        self.interactor = interactor
    }
    
    // MARK: -
    
    private func prepareComponents(for viewRecord: PechenkaViewRecord) -> [Component] {
        return viewRecord.view.compactMap { value in viewRecord.components.first { $0.type == value } }
    }
    
    private func showError(error: FetchError) {
        
        let title: String
        switch error {
        case .error(_): title = "Ошибка"
        case .incorrectAnswer: title = "Неверный ответ"
        case .incorrectUrl(_): title = "Неверный адрес"
        }
        
        let message: String
        switch error {
        case let .error(error): message = "\(error)"
        case .incorrectAnswer: message = "Внутренняя ошибка"
        case let .incorrectUrl(url): message = "\(url)"
        }
        
        self.viewInput?.showMessage(title: title, message: message, hideTimeout: nil)
        
    }
    
}

extension PechenkaPresenter: PechenkaViewControllerOutput {

    func viewDidLoad() {
        
        self.viewInput?.networkRequestStarted()
        
        self.interactor.fetch(then: { [weak self] result in
            
            DispatchQueue.main.async {
                self?.viewInput?.networkRequestFinished()
                
                guard let `self` = self else { return }
                
                switch result {
                case let .failure(error): self.showError(error: error)
                case let .success(viewRecord): self.viewInput?.displayComponents(components: self.prepareComponents(for: viewRecord))
                }
            }
            
        })
        
    }
    
    func componentTapped(data: [String : Any]?) {
        
        guard let data = data else { return }
        
        let title = "Tapped '\(data["name"] ?? "")' component"
        let message = "\(data)"
        
        self.viewInput?.showMessage(title: title, message: message, hideTimeout: 2.5)
        
    }
    
}
