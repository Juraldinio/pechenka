//
//  AppDelegate.swift
//  PechenkaAssigment
//
//  Created by Juraldinio on 01/04/2019.
//  Copyright © 2019 Juraldinio. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        self.window = UIWindow(frame: UIScreen.main.bounds)
        
        self.setupRootController()
        
        self.window?.makeKeyAndVisible()
        
        return true
    }

    private func setupRootController() {
        
        guard let window = self.window else { return }
        
        let interactor = PechenkaInteractor()
        let presenter = PechenkaPresenter(with: interactor)
        let viewcontroller = PechenkaViewController()
        
        viewcontroller.output = presenter
        presenter.viewInput = viewcontroller
        
        window.rootViewController = viewcontroller
        
    }

}

