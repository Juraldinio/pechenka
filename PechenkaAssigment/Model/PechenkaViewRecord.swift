//
//  PechenkaViewRecord.swift
//  PechenkaAssigment
//
//  Created by Juraldinio on 01/04/2019.
//  Copyright © 2019 Juraldinio. All rights reserved.
//

import Foundation

struct PechenkaViewRecord: Decodable {
    
    let components: [Component]
    let view: [String]
    
    enum CodingKeys: String, CodingKey {
        case components = "data"
        case view
    }
}
