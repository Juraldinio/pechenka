//
//  FetchError.swift
//  PechenkaAssigment
//
//  Created by Juraldinio on 01/04/2019.
//  Copyright © 2019 Juraldinio. All rights reserved.
//

import Foundation

enum FetchError: Error {
    case incorrectUrl(String)
    case error(Error)
    case incorrectAnswer
}
