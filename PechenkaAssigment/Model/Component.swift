//
//  Component.swift
//  PechenkaAssigment
//
//  Created by Juraldinio on 01/04/2019.
//  Copyright © 2019 Juraldinio. All rights reserved.
//

import Foundation

enum ComponentData: Decodable, Equatable {
    
    case string(String)
    case integer(Int)
    indirect case dictionary([String: ComponentData])
    indirect case array([ComponentData])
    
    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if let string = try? container.decode(String.self) {
            self = .string(string)
        } else if let integer = try? container.decode(Int.self) {
            self = .integer(integer)
        } else if let array = try? container.decode([ComponentData].self) {
            self = .array(array)
        } else {
            let dictionary = try container.decode([String: ComponentData].self)
            self = .dictionary(dictionary)
        }
    }
    
    public static func == (lhs: ComponentData, rhs: ComponentData) -> Bool {
        switch (lhs, rhs) {
        case (let .string(l), let .string(r)): return l == r
        case (let .integer(l), let .integer(r)): return l == r
        case (let .dictionary(l), let .dictionary(r)): return l == r
        case (let .array(l), let .array(r)): return l == r
        default:
            return false
        }
    }
    
}

typealias ComponentConfiguration = [String: ComponentData]

struct Component: Decodable {
    var type: String
    var configuration: ComponentConfiguration
    
    enum CodingKeys: String, CodingKey {
        case type = "name"
        case configuration = "data"
    }
}
