//
//  PechenkaViewController.swift
//  PechenkaAssigment
//
//  Created by Juraldinio on 01/04/2019.
//  Copyright © 2019 Juraldinio. All rights reserved.
//

import UIKit

protocol PechenkaViewControllerInput: AnyObject {
    func networkRequestStarted()
    func networkRequestFinished()
    func showMessage(title: String, message: String, hideTimeout: TimeInterval?)
    func displayComponents(components: [Component])
}

protocol PechenkaViewControllerOutput: AnyObject {
    func viewDidLoad()
    func componentTapped(data: [String: Any]?)
}

class PechenkaViewController: UIViewController {
    
    var output: PechenkaViewControllerOutput?
    private var stackView: UIStackView?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.output?.viewDidLoad()
        
        self.setupStackView()
        
        self.view.backgroundColor = .white

    }
    
    private lazy var activity: UIActivityIndicatorView = {
        let activity = UIActivityIndicatorView(style: .gray)
        activity.hidesWhenStopped = true
        activity.translatesAutoresizingMaskIntoConstraints = false
        activity.isUserInteractionEnabled = false
        
        self.view.addSubview(activity)
        
        NSLayoutConstraint.activate([
            activity.centerXAnchor.constraint(equalTo: self.view.centerXAnchor),
            activity.centerYAnchor.constraint(equalTo: self.view.centerYAnchor)
            ])
        
        return activity
    }()
    
    private func setupStackView() {
        let stackView = UIStackView(frame: self.view.bounds)
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.spacing = 5
        stackView.alignment = .center
        stackView.distribution = .equalCentering
        self.view.addSubview(stackView)
        NSLayoutConstraint.activate([
            stackView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor),
            stackView.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor),
            stackView.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor),
            stackView.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor)
            ])
        
        self.stackView = stackView
    }
    
    static private func createComponents(with configuration: [Component]) -> [ComponentView] {
        return configuration.compactMap { component -> ComponentView? in
            switch component.type {
            case "hz": return ComponentHZ(with: component.configuration)
            case "picture": return ComponentPicture(with: component.configuration)
            case "selector": return ComponentSelector(with: component.configuration)
            default:
                return nil
            }
        }
    }

}

extension PechenkaViewController: PechenkaViewControllerInput {
    
    func showMessage(title: String, message: String, hideTimeout: TimeInterval?) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        DispatchQueue.main.async { [weak self] in
            self?.present(alert, animated: true) {
                if let time = hideTimeout {
                    DispatchQueue.main.asyncAfter(deadline: .now() + time) {
                        alert.dismiss(animated: true, completion: nil)
                    }
                }
            }
        }
        
        
    }
    
    func displayComponents(components: [Component]) {
        PechenkaViewController
            .createComponents(with: components)
            .forEach {
                $0.delegate = self
                self.stackView?.addArrangedSubview($0.view)
        }
    }
    
    func networkRequestStarted() {
        self.stackView?.subviews.forEach { $0.removeFromSuperview() }
        self.activity.startAnimating()
    }
    
    func networkRequestFinished() {
        self.activity.stopAnimating()
    }
    
}

extension PechenkaViewController: ComponentViewDelegate {
    
    func componentTapped(component: ComponentView) {
        self.output?.componentTapped(data: component.currentData)
    }
    
}
