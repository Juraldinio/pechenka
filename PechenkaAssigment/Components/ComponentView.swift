//
//  ComponentView.swift
//  PechenkaAssigment
//
//  Created by Juraldinio on 01/04/2019.
//  Copyright © 2019 Juraldinio. All rights reserved.
//

import Foundation
import UIKit

protocol ComponentViewDelegate: AnyObject {
    func componentTapped(component: ComponentView)
}

protocol ComponentView: AnyObject {
    init(with configuration: ComponentConfiguration)
    var view: UIView { get }
    var delegate: ComponentViewDelegate? { get set }
    var currentData: [String: Any]? { get }
}
