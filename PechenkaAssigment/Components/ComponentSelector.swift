//
//  ComponentSelector.swift
//  PechenkaAssigment
//
//  Created by Jura on 4/1/19.
//  Copyright © 2019 Juraldinio. All rights reserved.
//

import UIKit

struct Variant {
    let id: Int
    let text: String
}


final class ComponentSelector: UIView, ComponentView {
    
    private var selectedID = 0 {
        didSet {
            self.label.text = self.variants.first { $0.id == self.selectedID }?.text ?? ""
        }
    }
    private var variants = [Variant]()
    
    private let label: UILabel
    private let picker: UIPickerView
    
    override init(frame: CGRect) {
        self.label = UILabel(frame: .zero)
        self.picker = UIPickerView(frame: .zero)
        super.init(frame: frame)
        self.translatesAutoresizingMaskIntoConstraints = false
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapped))
        self.addGestureRecognizer(tap)
    }
    
    required convenience init(with configuration: ComponentConfiguration) {
        self.init(frame: .zero)
        self.configureModel(with: configuration)
        self.configureLabel(with: configuration)
        self.configurePicker(with: configuration)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    // MARK: - Configure
    
    private func configureModel(with configuration: ComponentConfiguration) {

        guard let array = configuration["variants"]
            , case .array(let variants) = array else {
                return
        }
        
        self.variants = variants.compactMap {
            guard case let .dictionary(dictionary) = $0
                , case .integer(let id)? = dictionary["id"]
                , case .string(let text)? = dictionary["text"] else {
                    return nil
            }
            return Variant(id: id, text: text)
        }
        
        if let integer = configuration["selectedId"]
            , case .integer(let id) = integer {
            self.selectedID = id
        }
        
    }
    
    private func configureLabel(with configuration: ComponentConfiguration) {

        self.addSubview(self.label)
        self.label.translatesAutoresizingMaskIntoConstraints = false
        
        self.label.textAlignment = .center
        
        NSLayoutConstraint.activate([
            self.label.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            self.label.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            self.label.topAnchor.constraint(equalTo: self.topAnchor),
            self.label.heightAnchor.constraint(greaterThanOrEqualToConstant: 25)
            ])

    }
    
    private func configurePicker(with configuration: ComponentConfiguration) {
        
        self.addSubview(self.picker)
        self.picker.translatesAutoresizingMaskIntoConstraints = false
        
        self.picker.dataSource = self
        self.picker.delegate = self
        
        NSLayoutConstraint.activate([
            self.picker.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            self.picker.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            self.picker.topAnchor.constraint(equalTo: self.label.bottomAnchor),
            self.picker.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            ])
        
        let index = self.variants.firstIndex { $0.id == self.selectedID } ?? 0
        self.picker.selectRow(index, inComponent: 0, animated: true)
        
    }
    
    @objc
    func tapped() {
        self.delegate?.componentTapped(component: self)
    }
    
    // MARK: - ComponentView
    
    var view: UIView {
        return self
    }
    
    weak var delegate: ComponentViewDelegate?
    
    var currentData: [String : Any]? {
        return ["name": "selector", "text": self.label.text ?? "", "selectedID": self.selectedID]
    }
    
}


extension ComponentSelector: UIPickerViewDelegate {
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return self.variants[row].text
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.selectedID = self.variants[row].id
    }
    
}

extension ComponentSelector: UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.variants.count
    }
    
}
