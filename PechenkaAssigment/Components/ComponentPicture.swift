//
//  ComponentPicture.swift
//  PechenkaAssigment
//
//  Created by Juraldinio on 01/04/2019.
//  Copyright © 2019 Juraldinio. All rights reserved.
//

import Foundation
import UIKit

final class ComponentPicture: UIStackView, ComponentView {
    
    private let label: UILabel
    private var imagePath = ""
    private let picture: UIImageView
    
    override init(frame: CGRect) {
        self.label = UILabel(frame: .zero)
        self.picture = UIImageView(frame: .zero)
        super.init(frame: frame)
        
        self.spacing = 2
        self.alignment = .center
        self.axis = .vertical
        self.translatesAutoresizingMaskIntoConstraints = false
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapped))
        self.addGestureRecognizer(tap)
    }
    
    required convenience init(with configuration: ComponentConfiguration) {
        self.init(frame: .zero)
        self.configureLabel(with: configuration)
        self.configureImage(with: configuration)
    }
    
    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
        
    // MARK: - Configure
    
    private func configureLabel(with configuration: ComponentConfiguration) {
        
        guard let value = configuration["text"]
            , case let .string(text) = value else {
                return
        }
        
        self.addArrangedSubview(self.label)
    
        self.label.textAlignment = .center
        self.label.translatesAutoresizingMaskIntoConstraints = false
        self.label.text = text
        
        NSLayoutConstraint.activate([
            self.label.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            self.label.leadingAnchor.constraint(equalTo: self.leadingAnchor),
        ])
    
    }
    
    private func configureImage(with configuration: ComponentConfiguration) {
    
        guard let value = configuration["url"]
            , case let .string(path) = value
            , let url = URL(string: path) else {
                return
        }
        
        self.imagePath = path
    
        self.addArrangedSubview(self.picture)
        
        self.picture.contentMode = .scaleAspectFit
        self.picture.translatesAutoresizingMaskIntoConstraints = false
        
        DispatchQueue.global().async { [weak self] in
            let image: UIImage?
            do {
                let data = try Data(contentsOf: url)
                image = UIImage(data: data)
            } catch {
                return
            }
        
            DispatchQueue.main.async {
                self?.picture.image = image
            }
        
        }
        
        NSLayoutConstraint.activate([
            self.picture.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            self.picture.leadingAnchor.constraint(equalTo: self.leadingAnchor),
        ])
    
    }
    
    @objc
    func tapped() {
        self.delegate?.componentTapped(component: self)
    }
    
    // MARK: - ComponentView
    
    var view: UIView {
        return self
    }
    
    weak var delegate: ComponentViewDelegate?
    
    var currentData: [String : Any]? {
        return ["name": "picture", "text": self.label.text ?? "", "url": self.imagePath]
    }
    
}
