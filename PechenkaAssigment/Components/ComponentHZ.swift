//
//  ComponentHZ.swift
//  PechenkaAssigment
//
//  Created by Juraldinio on 01/04/2019.
//  Copyright © 2019 Juraldinio. All rights reserved.
//

import UIKit

final class ComponentHZ: UIView, ComponentView {
    
    private let label: UILabel
    
    override init(frame: CGRect) {
        self.label = UILabel(frame: .zero)
        super.init(frame: frame)
        self.translatesAutoresizingMaskIntoConstraints = false
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapped))
        self.addGestureRecognizer(tap)
    }
    
    required convenience init(with configuration: ComponentConfiguration) {
        self.init(frame: .zero)
        self.configureLabel(with: configuration)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Configure
    
    private func configureLabel(with configuration: ComponentConfiguration) {
        
        guard let value = configuration["text"]
            , case let .string(text) = value else {
                return
        }
        
        self.addSubview(self.label)
        
        self.label.translatesAutoresizingMaskIntoConstraints = false
        self.label.text = text
        
        NSLayoutConstraint.activate([
            self.label.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            self.label.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            self.label.heightAnchor.constraint(equalTo: self.heightAnchor),
            ])

    }
    
    @objc
    func tapped() {
        self.delegate?.componentTapped(component: self)
    }
    
    // MARK: - ComponentView
    
    var view: UIView {
        return self
    }
    
    weak var delegate: ComponentViewDelegate?
    
    var currentData: [String : Any]? {
        return ["name": "hz", "text": self.label.text ?? ""]
    }

}
