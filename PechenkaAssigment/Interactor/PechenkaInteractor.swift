//
//  PechenkaInteractor.swift
//  PechenkaAssigment
//
//  Created by Juraldinio on 01/04/2019.
//  Copyright © 2019 Juraldinio. All rights reserved.
//

import Foundation

protocol PechenkaInteractorInput: AnyObject {
    func fetch(then completion: @escaping (Result<PechenkaViewRecord, FetchError>) -> Void)
    func cancel()
}

final class PechenkaInteractor: PechenkaInteractorInput {
    
    private var task: URLSessionDataTask?
    
    func fetch(then completion: @escaping (Result<PechenkaViewRecord, FetchError>) -> Void) {
        
        self.cancel()
        
        let path = "https://prnk.blob.core.windows.net/tmp/JSONSample.json"
        guard let url = URL(string: path) else {
            completion(.failure(.incorrectUrl(path)))
            return
        }
        
        self.task = URLSession.shared.dataTask(with: url) { [weak self] (data, response, error) in
            
            self?.task = nil
            
            if let err = error {
                completion(.failure(.error(err)))
                return
            }
            
            guard let rawData = data, rawData.count > 0 else {
                completion(.failure(.incorrectAnswer))
                return
            }
            
            let record: PechenkaViewRecord
            do {
                record = try JSONDecoder().decode(PechenkaViewRecord.self, from: rawData)
            } catch {
                completion(.failure(.incorrectAnswer))
                return
            }
            
            completion(.success(record))
            
        }
        
        self.task?.resume()
        
    }
    
    func cancel() {
        guard let task = self.task else { return }
        task.cancel()
        self.task = nil
    }
    
}
