//
//  PechenkaAssigmentTests.swift
//  PechenkaAssigmentTests
//
//  Created by Juraldinio on 01/04/2019.
//  Copyright © 2019 Juraldinio. All rights reserved.
//

import XCTest

class PechenkaAssigmentTests: XCTestCase {

    func testModelCoding() throws {
        
        let url = Bundle(for: PechenkaAssigmentTests.self).url(forResource: "answer", withExtension: "json")
        let data = try Data(contentsOf: url!)
        let record = try JSONDecoder().decode(PechenkaViewRecord.self, from: data)
        
        XCTAssertTrue(!record.view.isEmpty)
        XCTAssertEqual(record.view, ["hz", "selector", "picture", "hz"])
        
        let hz = record.components.first { $0.type == "hz" }!
        XCTAssertEqual(hz.configuration["text"], .string("тринитротолуол"))
        
        let pic = record.components.first { $0.type == "picture" }!
        XCTAssertEqual(pic.configuration["text"], .string("Голова"))
        XCTAssertEqual(pic.configuration["url"], .string("https://cdnbeta.pryaniky.com/content/img/opportunities_8_talents.png"))
        
        let selector = record.components.first { $0.type == "selector" }!
        XCTAssertEqual(selector.configuration["selectedId"], .integer(1))
        
        guard case .array(let variants) = selector.configuration["variants"]! else {
            XCTFail()
            return
        }
        
        XCTAssertEqual(variants.count, 3)
        let variant = variants.first {
            if case let .dictionary(dict) = $0 {
                return dict["id"] == .integer(0)
            }
            return false
        }
        
        XCTAssertNotNil(variant)
        
        guard case let .dictionary(dict)? = variant else {
            XCTFail()
            return
        }
        
        XCTAssertEqual(dict["text"], .string("Вариант 1"))
        
    }

}
